"use strict";

var plusBtn = document.querySelectorAll('.plus');
plusBtn.forEach(function (btn) {
  btn.addEventListener('click', function () {
    var likesResult = parseInt(document.getElementById("".concat(btn.dataset.id)).textContent);
    var newLikes = likesResult + 1;
    fetch("http://localhost:3000/article/".concat(btn.dataset.id), {
      method: 'PATCH',
      headers: {
        'Accept': 'application/json',
        'Content-Type': "application/json; charset=UTF-8"
      },
      body: JSON.stringify({
        'likes': newLikes
      })
    }).then(function () {
      location.reload();
    });
  });
});