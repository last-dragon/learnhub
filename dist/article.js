"use strict";

function getUrlParam() {
  var query = window.location.search.substring(1);
  var id = query.split('=');
  return Number(id[1]);
}
;
var getId = getUrlParam();
var article = document.getElementById('article');
fetch("http://localhost:3000/article/".concat(getId)).then(function (response) {
  return response.json();
}).then(function (item) {
  article.innerHTML = "\n    <h1 class=\"article__title\">".concat(item.title, "</h1>\n    <div class=\"article__content\">\n    ").concat(item.content, "\n    <img src=\"").concat(item.pic, "\" alt=\"").concat(item.title, "\" class=\"article-img\">\n    </div>\n    ");
});