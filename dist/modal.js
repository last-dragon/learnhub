"use strict";

function getModal(options) {
  //Функция формирования HTML-кода модального окна
  function createModalWindow(options) {
    var elemModal = document.createElement('div');
    var modalTemplate = "<div class=\"window\" data-dismiss=\"modal\">\n\t\t\t\t\t\t\t\t<div class=\"window__content\">\n\t\t\t\t\t\t\t\t\t<div class=\"window__header\">\n\t\t\t\t\t\t\t\t\t\t<div class=\"window__title\" data-modal=\"title\">{{title}}</div>\n\t\t\t\t\t\t\t\t\t\t<span class=\"window__btn-close\" data-dismiss=\"modal\" title=\"\u0417\u0430\u043A\u0440\u044B\u0442\u044C\">\xD7</span>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class=\"window__body\" data-modal=\"content\">{{content}}</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>";
    elemModal.classList.add('modal');
    var modalHTML = modalTemplate.replace('{{title}}', options.title || 'Новое окно');
    modalHTML = modalHTML.replace('{{content}}', options.content || '');
    elemModal.innerHTML = modalHTML;
    document.body.appendChild(elemModal);
    return elemModal;
  }

  //Функция отображения окна на странице
  var createModal;
  var eventShowModal;
  var hiding = false;
  var destroyed = false;
  function showModal() {
    if (!destroyed && !hiding) {
      createModal.classList.add('window__show');
      document.dispatchEvent(eventShowModal);
    }
  }

  //Функция скрытия окна на странице
  var eventHideModal;
  function hideModal() {
    var animationSpeed = 200;
    hiding = true;
    createModal.classList.remove('window__show');
    createModal.classList.add('window_hiding');
    setTimeout(function () {
      createModal.classList.remove('window_hiding');
      hiding = false;
    }, animationSpeed);
    document.dispatchEvent(eventHideModal);
  }

  //Функция закрытия окна по крестику или по клику вне окна
  function handlerCloseModal(e) {
    if (e.target.dataset.dismiss === 'modal') {
      hideModal();
    }
  }
  createModal = createModalWindow(options || {});
  createModal.addEventListener('click', handlerCloseModal);
  eventShowModal = new CustomEvent('show.modal', {
    detail: createModal
  });
  eventHideModal = new CustomEvent('hide.modal', {
    detail: createModal
  });
  return {
    show: showModal,
    hide: hideModal,
    destroy: function destroy() {
      createModal.parentElement.removeChild(createModal), createModal.removeEventListener('click', handlerCloseModal), destroyed = true;
    },
    setContent: function setContent(html) {
      createModal.querySelector('[data-modal="content"]').innerHTML = html;
    },
    setTitle: function setTitle(text) {
      createModal.querySelector('[data-modal="title"]').innerHTML = text;
    }
  };
}
;