"use strict";

// Карточка главного элемента
function addMainCardToDocument(title, content, pic, id, likes) {
  var mainCardTemplate = document.getElementById('main-card-template');
  var firstSectionItemMain = document.querySelector('.first-section-item-main');
  var cloneMainCard = mainCardTemplate.content.cloneNode(true);
  cloneMainCard.querySelector('.first-section-item-main-title').textContent = title;
  cloneMainCard.querySelector('.first-section-item-main-text').textContent = content;
  cloneMainCard.querySelector('.first-section-item-main-img').src = pic;
  cloneMainCard.querySelector('.first-section-item-main-img').alt = title;
  cloneMainCard.querySelector('.link-mainCard').href = "article.htm?id=".concat(id);
  cloneMainCard.querySelector('.card-end-text').textContent = likes;
  cloneMainCard.querySelector('.card-end-text').id = "".concat(id);
  cloneMainCard.querySelector('.plus').dataset.id = "".concat(id);
  cloneMainCard.querySelector('.minus').dataset.id = "".concat(id);
  firstSectionItemMain.append(cloneMainCard);
}
;

// Карточки первой секции
function addFirstCardToDocument(title, content, pic, id, likes) {
  var firstCardTemplate = document.querySelector('.first-section-template');
  var firstSectionItem = document.querySelector('.first-section');
  var cloneFirstCard = firstCardTemplate.content.cloneNode(true);
  cloneFirstCard.querySelector('.first-section-item-title').textContent = title;
  cloneFirstCard.querySelector('.first-section-item-text').textContent = content;
  cloneFirstCard.querySelector('.first-section-item-img').src = pic;
  cloneFirstCard.querySelector('.first-section-item-img').alt = title;
  cloneFirstCard.querySelector('.link-firstCard').href = "article.htm?id=".concat(id);
  cloneFirstCard.querySelector('.card-end-text').textContent = likes;
  cloneFirstCard.querySelector('.card-end-text').id = "".concat(id);
  cloneFirstCard.querySelector('.plus').dataset.id = "".concat(id);
  cloneFirstCard.querySelector('.minus').dataset.id = "".concat(id);
  firstSectionItem.append(cloneFirstCard);
}

// Карточки второй секции
function addSecondCardToDocument(title, content, pic, id, likes) {
  var secondCardTemplate = document.querySelector('.section-section-template');
  var secondSectionItem = document.querySelector('.second-section');
  var cloneSecondCard = secondCardTemplate.content.cloneNode(true);
  cloneSecondCard.querySelector('.second-section-title').textContent = title;
  cloneSecondCard.querySelector('.second-section-text').textContent = content;
  cloneSecondCard.querySelector('.second-section-item-img').src = pic;
  cloneSecondCard.querySelector('.second-section-item-img').alt = title;
  cloneSecondCard.querySelector('.link-secondCard').href = "article.htm?id=".concat(id);
  cloneSecondCard.querySelector('.card-end-text').textContent = likes;
  cloneSecondCard.querySelector('.card-end-text').id = "".concat(id);
  cloneSecondCard.querySelector('.plus').dataset.id = "".concat(id);
  cloneSecondCard.querySelector('.minus').dataset.id = "".concat(id);
  secondSectionItem.append(cloneSecondCard);
}

// Функция прибавления лайков
function plusLikes() {
  var plusBtn = document.querySelectorAll('.plus');
  plusBtn.forEach(function (btn) {
    btn.addEventListener('click', function () {
      var likesResult = parseInt(document.getElementById("".concat(btn.dataset.id)).textContent);
      var newLikes = likesResult + 1;
      fetch("http://localhost:3000/article/".concat(btn.dataset.id), {
        method: 'PATCH',
        headers: {
          'Accept': 'application/json',
          'Content-Type': "application/json; charset=UTF-8"
        },
        body: JSON.stringify({
          'likes': newLikes
        })
      }).then(function (response) {
        return response.json();
      }).then(function (article) {
        document.getElementById("".concat(btn.dataset.id)).textContent = '';
        document.getElementById("".concat(btn.dataset.id)).textContent = article.likes;
      });
    });
  });
}

// Функция удаления лайков
function minusLikes() {
  var minusBtn = document.querySelectorAll('.minus');
  minusBtn.forEach(function (btn) {
    btn.addEventListener('click', function () {
      var likesResult = parseInt(document.getElementById("".concat(btn.dataset.id)).textContent);
      var newLikes = likesResult - 1;
      fetch("http://localhost:3000/article/".concat(btn.dataset.id), {
        method: 'PATCH',
        headers: {
          'Accept': 'application/json',
          'Content-Type': "application/json; charset=UTF-8"
        },
        body: JSON.stringify({
          'likes': newLikes
        })
      }).then(function (response) {
        return response.json();
      }).then(function (article) {
        document.getElementById("".concat(btn.dataset.id)).textContent = '';
        document.getElementById("".concat(btn.dataset.id)).textContent = article.likes;
      });
    });
  });
}

// Вызов элементов и помещение их в соответствующие карточки
var getAllCard = function getAllCard() {
  fetch('http://localhost:3000/article/').then(function (response) {
    return response.json();
  }).then(function (articles) {
    articles.slice().reverse().forEach(function (article, i) {
      if (i == 0) {
        addMainCardToDocument(article.title, article.content, article.pic, article.id, article.likes);
      } else if (i > 0 && i < 5) {
        addFirstCardToDocument(article.title, article.content, article.pic, article.id, article.likes);
      } else if (i > 4 && i < 11) {
        addSecondCardToDocument(article.title, article.content, article.pic, article.id, article.likes);
      } else if (i > 10 && i < 21) {
        document.querySelector('.more').addEventListener('click', function () {
          addSecondCardToDocument(article.title, article.content, article.pic, article.id, article.likes);
          document.querySelector('.more').style.display = 'none';
          // Добавляем и удаляем лайки в дополнительном списке
          plusLikes();
          minusLikes();
        });
      }
    });
    // Добавляем и удаляем лайки
    plusLikes();
    minusLikes();
  })["catch"](function (err) {
    alert('Нет статей');
    console.log("ERR: ", err);
  });
};
getAllCard();