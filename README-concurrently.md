Пакет concurrently для одновременного запуска нескольких процессов
Глобальная установка: npm i -g concurrently
Использовал в package.json: "start": "concurrently \"json-server -w db.json\" \"gulp watch\"" 
Теперь при команде npm start одновременно запускается json-server и gulp watch