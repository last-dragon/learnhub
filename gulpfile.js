const gulp = require('gulp');
const babel = require('gulp-babel');
const sass = require('gulp-sass')(require('sass'));

gulp.task("js", ()=> {
    return gulp.src("js/*.js")
    .pipe(babel({
        presets: ['@babel/env']
    }))
        .pipe(gulp.dest("dist/"))
})

gulp.task("style", ()=> {
    return gulp.src("styles/index.scss")
    .pipe(sass())
        .pipe(gulp.dest("styles/"))
})

gulp.task("watch", ()=> {
    return gulp.watch(["js/*.js", "styles/index.scss"], gulp.parallel(["js", "style"]))
})